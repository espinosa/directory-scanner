package org.bitbucket.espinosa.directoryscanner;

import static org.bitbucket.espinosa.directoryscanner.Collectors.asList;
import static org.bitbucket.espinosa.directoryscanner.Matchers.directoryWhere;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fnEq;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fnGlob;
import static org.bitbucket.espinosa.directoryscanner.Matchers.hasFiles;
import static org.bitbucket.espinosa.directoryscanner.Matchers.hasNamesLike;
import static org.bitbucket.espinosa.directoryscanner.util.ScannerResultFormatter.format;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.dir;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.file;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.recreateDir;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.file.Path;
import java.util.List;

import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.Dir;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 * Unit tests for {@link DirectoryScanner}.
 * 
 * @author Espinosa
 */
public class ScannerTest {
	/**
	 * For each test is creates temporary directory within system global
	 * temporary directory - in Windows OS it is something like:
	 * {@code C:\Users\Espinosa\AppData\Local\Temp\junit2454273810127757546\} -
	 * which is then deleted after every test.
	 */
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();
	
	String testFolder;
	Path testFolderPath;

	/**
	 * Create directory and file tree structure resembling application
	 * installation directories in OS Windows. Place reduced imitations of
	 * "Libre Office 5" and Java JDK and JRE typical installations - just file
	 * and directory names, no real content.
	 */
	@Before
	public void setup() throws Exception {
		Dir d = dir("C",
			dir("Program Files (x86)",
				dir("Mozilla Firefox"),
				dir("Mozilla Thunderbird"),
				dir("LibreOffice 5",
					file("CREDITS.fodt"),
					file("LICENSE.html"),  
					dir("help"),        
					dir("readmes/"),
					file("LICENSE.fodt"),  
					file("license.txt"),  
					dir("program",
						dir("classes", 
							file("unoil.jar"),
							file("ridl.jar"),
							file("juh.jar"),
							file("jurt.jar")
								),  
						dir("share",
							dir("autocorr"),
							dir("config", 
								file("images_breeze.zip"),
								dir("soffice.cfg",
									dir("formula", 
										dir("ui", 
											file("structpage.ui"))))),
							dir("gallery")))),
				dir("LibreOffice_5.0_SDK"),
				dir("Java",
					dir("jre1.8.0_51"),
					dir("jdk1.8.0_51",
						dir("jre"),     
						file("src.zip"),
						file("LICENSE"),
						dir("lib"),
						dir("bin",
							file("java.exe"),
							file("javac.exe")))),
				dir("scala"),
				dir("VideoLAN/")));
		recreateDir(tempFolder, d);
		
		testFolder = tempFolder.getRoot().getAbsolutePath();
		testFolderPath = tempFolder.getRoot().toPath();
	}

	/**
	 * Scan for "Libre Office 5" installation directory containing certain JAR
	 * in test files system reflecting (very partially) real Windows OS
	 * installation directory setup.
	 */
	@Test
	public void testSingleRoot() {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.root(testFolder)
				.find(
					directoryWhere(
						fnEq("LibreOffice 5"),
						hasFiles(fnEq("unoil.jar"), fnEq("ridl.jar"), fnEq("juh.jar"), fnEq("jurt.jar"))));
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("" +
				"/C/Program Files (x86)/LibreOffice 5\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/juh.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/jurt.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/ridl.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/unoil.jar", 
				format(sr, testFolderPath));
	}

	/**
	 * Scan for "Libre Office 5" installation directory containing certain JAR
	 * in test files system and
	 * 
	 * The test directory root is guaranteed to contain searched item, the real
	 * application directories may also contain searched item so we look for
	 * 1..N result with at leat one expected.
	 * 
	 * Note: this test may take quite long to run!
	 */
	@Test
	public void testMultipleRootsAndGlobPatterns() {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.roots("C:/Program Files ZZZZ",     // Windows OS 64bit application installation directory
					"C:/Program Files (x86) ZZZZ", // Windows OS 32bit application installation directory
					"/usr ZZZ",  // Linux official application installation directory
					"/opt ZZZ",  // Linux alternative application installation directory
					testFolder,
					"/Applications ZZZ")  // Mac OS X
				.find(
					directoryWhere(
					hasNamesLike(fnGlob("libreoffice*"), fnGlob("openoffice*")),
					hasFiles(fnEq("unoil.jar"), fnEq("ridl.jar"), fnEq("juh.jar"), fnEq("jurt.jar")))
				);
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("" +
				"/C/Program Files (x86)/LibreOffice 5\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/juh.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/jurt.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/ridl.jar\n" + 
				"> /C/Program Files (x86)/LibreOffice 5/program/classes/unoil.jar", 
				format(sr, testFolderPath));
	}
}
