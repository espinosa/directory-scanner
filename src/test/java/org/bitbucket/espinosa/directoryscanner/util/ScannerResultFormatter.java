package org.bitbucket.espinosa.directoryscanner.util;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.bitbucket.espinosa.directoryscanner.result.MultiPathResult;
import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;

/**
 * Provide convenient to text renderer for scanner results, suitable for testing.
 * Uses multi-line output for easy text comparison.
 * 
 * Example (complex results):
 * 
 * <pre>
 * {@code
 * /a 
 * /a/b1 
 * /a/b2/c2 
 * }
 * </pre>
 * 
 * Example (complex results with sub-results):
 * <pre>
 * {@code
 * /C:/Program Files (x86)/LibreOffice 5
 * > /C:/Program Files (x86)/LibreOffice 5/program/classes/juh.jar
 * > /C:/Program Files (x86)/LibreOffice 5/program/classes/jurt.jar
 * > /C:/Program Files (x86)/LibreOffice 5/program/classes/ridl.jar
 * > /C:/Program Files (x86)/LibreOffice 5/program/classes/unoil.jar
 * }
 * </pre>
 * 
 * If root directory is present (not null), then all paths in result are "relativized" to it.
 * This is essential for unit testing so it can run everywhere.
 * Example:
 * {@code C:\\Users\\Espinosa\\AppData\\Local\\Temp\\junit2454273810127757546\\a\\b1\\c2}
 * become
 * {@code /a/b2/c2}. Neat!
 * 
 * @author Espinosa
 */
public class ScannerResultFormatter {
	
	public static String format(Collection<ScannerResult> results) {
		return format(results, null);
	}
	
	public static String format(Collection<ScannerResult> results, Path rootDir) {
		StringBuffer sb = new StringBuffer();
		int i = 0;
		for (ScannerResult r : results) {
			if (i++ > 0) {
				sb.append("\n");
			}
			sb.append(format(r, rootDir));
		}
		return sb.toString();
	}
	
	public static String format(ScannerResult sr, Path rootDir) {
		StringBuffer sb = new StringBuffer();
		sb.append(renderPath(sr.getPath(), rootDir));
		
		if (sr instanceof MultiPathResult) {
			MultiPathResult sra = (MultiPathResult)sr;
			int i = 0;
			if (sra.getSubPaths() instanceof List) {
				((List<Path>)sra.getSubPaths()).sort(new PathComparator());
			}
			for (Path p : sra.getSubPaths()) {
				if (i++ > 0) {
					sb.append("\n");
				} else {
					sb.append("\n");
				}
				sb.append("> ");
			    sb.append(renderPath(p, rootDir));
			}
		}
		return sb.toString();
	}

	private static String renderPath(Path path, Path rootDir) {
		if (rootDir != null) 
		    return renderPath(rootDir.relativize(path));
		else 
			return renderPath(path);		
	}
	
	private static String renderPath(Path path) {
		StringBuffer sb = new StringBuffer();
		if (path.getRoot() != null) {
			sb.append("/");
			sb.append(path.getRoot().toString().replaceAll("/", "").replaceAll("\\\\", ""));
		}
		for (Path pathSegment : path) {
			sb.append("/");
			sb.append(pathSegment.toString());
		}
		return sb.toString();
	}
	
	public static class PathComparator implements Comparator<Path> {
		@Override
		public int compare(Path p1, Path p2) {
			return p1.compareTo(p2);
		}	
	}
}
