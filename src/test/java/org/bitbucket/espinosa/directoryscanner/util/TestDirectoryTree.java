package org.bitbucket.espinosa.directoryscanner.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.junit.rules.TemporaryFolder;

public class TestDirectoryTree {
	
	public static void recreateDir(TemporaryFolder tempDir, FsNode n) throws IOException {
		recreateDir(tempDir.getRoot().toPath(), n);
	}

	private static void recreateDir(Path parent, FsNode n) throws IOException {
		if (n instanceof File) {
			Files.createFile(parent.resolve(n.getName()));	
		} else if (n instanceof Dir) {
			Path d = Files.createDirectory(parent.resolve(n.getName()));
			for (FsNode child : n.getChildren()) {
				recreateDir(d, child);
			}
		}
	}

	public static interface FsNode {
		String getName();
		List<FsNode> getChildren();
	}

	public static class Dir implements FsNode {
		private final String name;
		private final List<FsNode> children;

		public Dir(String name, List<FsNode> children) {
			this.name = name;
			this.children = children;
		}

		public String getName() {
			return name;
		}
		public List<FsNode> getChildren() {
			return children;
		}
	}

	public static class File implements FsNode {
		private final String name;
		private final static List<FsNode> EMPTY_LIST = new LinkedList<>(); 

		public File(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
		public List<FsNode> getChildren() {
			return EMPTY_LIST;
		}
	}

	public static Dir dir(String name, FsNode... nodes) {
		return new Dir(name, Arrays.asList(nodes));
	}

	public static File  file(String name) {
		return new File(name);
	}
}
