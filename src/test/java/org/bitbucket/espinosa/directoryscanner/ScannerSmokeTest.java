package org.bitbucket.espinosa.directoryscanner;

import static org.bitbucket.espinosa.directoryscanner.Collectors.asList;
import static org.bitbucket.espinosa.directoryscanner.Matchers.directoryWhere;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fnEq;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fnGlob;
import static org.bitbucket.espinosa.directoryscanner.Matchers.hasFiles;
import static org.bitbucket.espinosa.directoryscanner.Matchers.hasNamesLike;
import static org.bitbucket.espinosa.directoryscanner.util.ScannerResultFormatter.format;

import java.util.List;

import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This smoke test runs against real file system. It may not work properly on other 
 * box then mine. Look into {@link ScannerTest} for a proper unit test.
 * 
 * TODO: extempt this test from normal Maven build
 * 
 * @author Espinosa
 */
public class ScannerSmokeTest {
	
	private final Logger log = LoggerFactory.getLogger(ScannerTest.class);

    // find 'C:\Program Files (x86)\LibreOffice 5'
    // in my case it is:
    // LibreOffice.installation.dir=C:/Program Files (x86)/LibreOffice 5
    //    LibreOffice.sdk.lib.unoil=/program/classes/unoil.jar
    //    LibreOffice.sdk.lib.ridl=/program/classes/ridl.jar
    //    LibreOffice.sdk.lib.juh=/program/classes/juh.jar
    //    LibreOffice.sdk.lib.jurt=/program/classes/jurt.jar
    
    @Test
    public void testSingleRoot() {
    	DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
                .roots("C:/Program Files (x86)")
                .find(
                directoryWhere(
                  fnEq("LibreOffice 5"),
                  hasFiles(fnEq("unoil.jar"), fnEq("ridl.jar"), fnEq("juh.jar"), fnEq("jurt.jar"))
                )
              );
    	List<ScannerResult> sr = s.collect(asList());
        log.info("Result: " + format(sr));
    }
    
    @Test
    public void testMultipleRootsAndGlobPatterns() {
    	DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
                .roots("C:/Program Files", 
                        "C:/Program Files (x86)", 
                        "/opt", 
                        "/usr", 
                        "/Applications")
                .find(
                directoryWhere(
                	hasNamesLike(fnGlob("libreoffice*"), fnGlob("openoffice*")),
                	hasFiles(fnEq("unoil.jar"), fnEq("ridl.jar"), fnEq("juh.jar"), fnEq("jurt.jar")))
                );
    	List<ScannerResult> sr = s.collect(asList());
        log.debug("Result: " + sr);
    }
    
    @Test
    public void crashTest() {
    	DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
                .roots("C:/Program Files (x86)/Google")
                .find(
                directoryWhere(
                	fnGlob("LibreOffice 5"),
                	hasFiles(fnGlob("unoil.jar"), fnGlob("ridl.jar"), fnGlob("juh.jar"), fnGlob("jurt.jar")))
                );
        List<ScannerResult> sr = s.collect(asList());
        log.debug("Result: " + sr);
    }
}
