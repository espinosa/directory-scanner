package org.bitbucket.espinosa.directoryscanner;

import static org.bitbucket.espinosa.directoryscanner.Collectors.asList;
import static org.bitbucket.espinosa.directoryscanner.Matchers.directoryWhere;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fileWhere;
import static org.bitbucket.espinosa.directoryscanner.Matchers.fnEq;
import static org.bitbucket.espinosa.directoryscanner.Matchers.hasNamesLike;
import static org.bitbucket.espinosa.directoryscanner.Matchers.pathWhere;
import static org.bitbucket.espinosa.directoryscanner.util.ScannerResultFormatter.format;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.dir;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.file;
import static org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.recreateDir;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.nio.file.Path;
import java.util.List;

import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.bitbucket.espinosa.directoryscanner.util.TestDirectoryTree.Dir;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

public class SimpleScannerTest {

	/**
	 * For each test is creates temporary directory within system global
	 * temporary directory - in Windows OS it is something like:
	 * {@code C:\Users\Espinosa\AppData\Local\Temp\junit2454273810127757546\} -
	 * which is then deleted after every test.
	 */
	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	String testFolder;
	Path testFolderPath;

	@Before
	public void setup() throws Exception {
		Dir d = dir("a",
			dir("b1"),
			dir("b2",
				file("c1"),
				file("c2")),
			dir("b3"));
		recreateDir(tempFolder, d);

		testFolder = tempFolder.getRoot().getAbsolutePath();
		testFolderPath = tempFolder.getRoot().toPath();
	}

	@Test
	public void findRootByName() throws Exception {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.roots(testFolder)
				.find(
					directoryWhere(fnEq("a")));
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("/a", format(sr, testFolderPath));
	}

	@Test
	public void findSingleFileByName() throws Exception {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.roots(testFolder)
				.find(
					fileWhere(fnEq("c2")));
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("/a/b2/c2", format(sr, testFolderPath));
	}

	@Test
	public void findSingleDirectoryByName() throws Exception {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.roots(testFolder)
				.find(
					directoryWhere(fnEq("b2")));
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("/a/b2", format(sr, testFolderPath));
	}

	@Test
	public void findFileOrDirectoryByName() throws Exception {
		DirectoryScanner<ScannerResult> s = ScannerBuilder.get()
				.roots(testFolder)
				.find(
					pathWhere(hasNamesLike(fnEq("a"), fnEq("b1"), fnEq("c2"))));
		List<ScannerResult> sr = s.collect(asList());

		assertNotNull(sr);
		assertEquals("" +
				"/a\n" + 
				"/a/b1\n" + 
				"/a/b2/c2", 
				format(sr, testFolderPath));
	}
}
