package org.bitbucket.espinosa.directoryscanner;

import java.nio.file.Path;

import org.bitbucket.espinosa.directoryscanner.visitor.CollectingVisitor;
import org.bitbucket.espinosa.directoryscanner.visitor.Collector;

/**
 * Core class. Execute scanning recipe. 
 * 
 * https://bitbucket.org/espinosa/DirectoryScanner
 * 
 * TODO: consider merging back DirectoryScannerCore and DirectoryScanner
 * 
 * @author Espinosa
 */
public class DirectoryScanner<T> extends DirectoryScannerCore {	
	private final Path[] rootPaths;
	private final CollectingVisitor<T> cv;

	public DirectoryScanner(Path[] rootPaths, CollectingVisitor<T> cv) {
		this.rootPaths = rootPaths; 
		this.cv = cv;
	}
	
	public <C> C collect(Collector<T, C> collector) {
		cv.setCollector(collector);
		for (Path rootPath : rootPaths) {
			scan(rootPath, cv);
		}
		return collector.results();
	}
}
