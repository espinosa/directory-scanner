package org.bitbucket.espinosa.directoryscanner;

import java.util.List;

import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.bitbucket.espinosa.directoryscanner.visitor.Collector;
import org.bitbucket.espinosa.directoryscanner.visitor.LinkedListCollector;

public class Collectors {
	
	/** define linked list based collector */
	public static Collector<ScannerResult, List<ScannerResult>> asList() {
		return new LinkedListCollector<ScannerResult, List<ScannerResult>>();
	}
}
