package org.bitbucket.espinosa.directoryscanner;

public class ScannerException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public ScannerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ScannerException(String message) {
		super(message);
	}

	public ScannerException(Throwable cause) {
		super(cause);
	}
}