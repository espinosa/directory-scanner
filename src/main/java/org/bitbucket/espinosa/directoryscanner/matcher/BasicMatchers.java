package org.bitbucket.espinosa.directoryscanner.matcher;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;

public class BasicMatchers {

	/** Exact equality matcher */
	public static class EqMatcher implements BooleanMatcher<Path> {
		private final String pattern;

		public EqMatcher(String pattern) {
			if (pattern == null) throw new IllegalArgumentException("Pattern cannot be null"); 
			this.pattern = pattern;
		}
		@Override public boolean match(Path p) {
			return pattern.equals(p.getFileName().toString());
		}
	}

	/** Full Regex template matching */
	public static class RegexMatcher implements BooleanMatcher<Path> {
		private final PathMatcher pattern;
		private static final FileSystem fs = FileSystems.getDefault();

		public RegexMatcher(String pattern) {
			if (pattern == null) throw new IllegalArgumentException("Pattern cannot be null");
			this.pattern = fs.getPathMatcher("glob:**/" + pattern);
		}
		@Override public boolean match(Path p) {
			return pattern.matches(p);
		}
	}

	/** Java 7 GLOB template matcher */
	public static class GlobMatcher implements BooleanMatcher<Path> {
		private final PathMatcher pattern;
		private static final FileSystem fs = FileSystems.getDefault();

		public GlobMatcher(String pattern) {
			if (pattern == null) throw new IllegalArgumentException("Pattern cannot be null");
			this.pattern = fs.getPathMatcher("glob:**/" + pattern);
		}
		@Override public boolean match(Path p) {
			return pattern.matches(p);
		}
	}
}
