package org.bitbucket.espinosa.directoryscanner.matcher;

import java.nio.file.Files;
import java.nio.file.Path;

public class IsFileMatcher implements BooleanMatcher<Path> {

	@Override
	public boolean match(Path p) {
		return Files.isRegularFile(p);
	}
}
