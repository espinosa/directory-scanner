package org.bitbucket.espinosa.directoryscanner.matcher;

public class AndGroupMatcher<T> implements GroupMatcher<T> {
	private final BooleanMatcher<T>[] matchers;
	
	@SafeVarargs
	public AndGroupMatcher(BooleanMatcher<T>... matchers) {
		this.matchers = matchers;
	}

	/**
	 * @return true if there is at least one matching
	 */
	@Override
	public boolean match(T p) {
		for (BooleanMatcher<T> m : matchers) {
			if (!m.match(p)) return false;
		}
		return true;
	}

	@Override
	public BooleanMatcher<T>[] getMatchers() {
		return matchers;
	}
}
