package org.bitbucket.espinosa.directoryscanner.matcher;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

import org.bitbucket.espinosa.directoryscanner.DirectoryScannerCore;
import org.bitbucket.espinosa.directoryscanner.visitor.LinkedListCollector;
import org.bitbucket.espinosa.directoryscanner.visitor.PathCollectingVisitor;

public class HasFilesMatcher implements BooleanMatcherWithResults<Path, Path> {
	private final SatisfiableBooleanMatcher<Path> matcher;
	private final DirectoryScannerCore scanner;
	private Collection<Path> results;
	
	public HasFilesMatcher(SatisfiableBooleanMatcher<Path> matcher) {
		this.matcher = matcher;
		this.scanner = new DirectoryScannerCore();
	}

	@Override
	public boolean match(Path p) {
		PathCollectingVisitor cv = new PathCollectingVisitor(matcher);		
		LinkedListCollector<Path, List<Path>> collector = new LinkedListCollector<>();
		cv.setCollector(collector);
		scanner.scan(p, cv);
		results = collector.results();
		return (matcher.allSatisfied(results));
	}
	
	@Override
	public Collection<Path> results() {
		return results;
	}
}
