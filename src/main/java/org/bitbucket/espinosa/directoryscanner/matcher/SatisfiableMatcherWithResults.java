package org.bitbucket.espinosa.directoryscanner.matcher;

/**
 * 
 * @author Espinosa
 *
 * @param <T> input type
 * @param <R> return type
 */
public interface SatisfiableMatcherWithResults <T, R> 
	extends BooleanMatcherWithResults<T, R>,  SatisfiableBooleanMatcher<T> {
}
