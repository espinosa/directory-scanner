package org.bitbucket.espinosa.directoryscanner.matcher;

import java.util.Collection;

import org.bitbucket.espinosa.directoryscanner.result.MultiPathResult;

/**
 * Boolean matcher that also collects some sub-results that are supposed to be
 * included in overall scan results. See {@link MultiPathResult#getSubPaths()}
 * 
 * @author Espinosa
 *
 * @param <T>
 *            input type
 * @param <R>
 *            return type of sub results
 */
public interface BooleanMatcherWithResults<T, R> extends BooleanMatcher<T> {
	public Collection<R> results();
}
