package org.bitbucket.espinosa.directoryscanner.matcher;

import java.nio.file.Files;
import java.nio.file.Path;

public class IsDirectoryMatcher implements BooleanMatcher<Path> {

	@Override
	public boolean match(Path p) {
		return Files.isDirectory(p);
	}

}
