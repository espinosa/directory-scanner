package org.bitbucket.espinosa.directoryscanner.matcher;

import java.util.Collection;

public interface SatisfiableBooleanMatcher<T> extends BooleanMatcher<T> {
	boolean allSatisfied(Collection<T> candidates);
}
