package org.bitbucket.espinosa.directoryscanner.matcher;

public interface BooleanMatcher<T> {
	boolean match(T p);
}
