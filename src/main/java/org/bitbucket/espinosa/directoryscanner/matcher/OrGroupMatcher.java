package org.bitbucket.espinosa.directoryscanner.matcher;

public class OrGroupMatcher<T> implements GroupMatcher<T> {
	private final BooleanMatcher<T>[] matchers;
	
	@SafeVarargs
	public OrGroupMatcher(BooleanMatcher<T>... matchers) {
		this.matchers = matchers;
	}

	/**
	 * @return true if there is at least one matching
	 */
	@Override
	public boolean match(T p) {
		for (BooleanMatcher<T> m : matchers) {
			if (m.match(p)) return true;
		}
		return false;
	}

	@Override
	public BooleanMatcher<T>[] getMatchers() {
		return matchers;
	}
}
