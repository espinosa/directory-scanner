package org.bitbucket.espinosa.directoryscanner.matcher;

import java.nio.file.Path;
import java.util.Collection;

/**
 * See {@link HasFilesMatcher}. 
 * 
 * @author Espinosa
 */
public class SatifiableGroupMatcher implements SatisfiableBooleanMatcher<Path> {
	private final BooleanMatcher<Path>[] matchers;

	@SafeVarargs
	public SatifiableGroupMatcher(BooleanMatcher<Path>... matchers) {
		this.matchers = matchers;
	}

	@Override
	public boolean match(Path p) {
		for (BooleanMatcher<Path> m : matchers) {
			if (m.match(p)) return true;
		}
		return false;
	}

	@Override
	public boolean allSatisfied(Collection<Path> candidates) {
		if (candidates.size() == 0) return false;

		for (BooleanMatcher<Path> matcher : matchers) {
			if (!isMatcherSatisfied(matcher, candidates)) {
				return false;
			}
		}
		return true;
	}

	private boolean isMatcherSatisfied(BooleanMatcher<Path> matcher, Collection<Path> candidates) {
		for (Path p : candidates) {
			if (matcher.match(p)) {
				return true;
			}
		}
		return false;
	}
}
