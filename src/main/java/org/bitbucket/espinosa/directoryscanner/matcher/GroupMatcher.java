package org.bitbucket.espinosa.directoryscanner.matcher;

public interface GroupMatcher<T> extends BooleanMatcher<T> {
	BooleanMatcher<T>[] getMatchers();
}
