package org.bitbucket.espinosa.directoryscanner;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.bitbucket.espinosa.directoryscanner.visitor.CollectingVisitor;

public class ScannerBuilder {
	private Path[] roots;
	
	public static ScannerBuilder get() {
		return new ScannerBuilder();
	}
	
	public ScannerBuilder roots(Path... paths) {
		this.roots = paths;
		return this;
	}
	
	public ScannerBuilder roots(String... paths) {
		this.roots = new Path[paths.length];
		for (int i = 0; i < paths.length; i++) {
			this.roots[i] = Paths.get(paths[i]);
		}
		return this;
	}
	
	public ScannerBuilder root(Path path) {
		this.roots = new Path[]{path};
		return this;
	}
	
	public ScannerBuilder root(String path) {
		this.roots = new Path[]{Paths.get(path)};
		return this;
	}
	
	public <T> DirectoryScanner<T> find(CollectingVisitor<T> cv) {
		return new DirectoryScanner<T>(roots, cv);
	}
}
