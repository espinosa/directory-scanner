package org.bitbucket.espinosa.directoryscanner;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumSet;

import org.bitbucket.espinosa.directoryscanner.visitor.CollectingVisitor;
import org.bitbucket.espinosa.directoryscanner.visitor.DelegatingVisitor;

public class DirectoryScannerCore {
	private int maxDepth = 20;
	
	public <T> void scan(Path rootPath, CollectingVisitor<T> cv) {
		if (!Files.exists(rootPath)) return;
		try {
			Files.walkFileTree(
				rootPath, 
				EnumSet.of(FileVisitOption.FOLLOW_LINKS), 
				maxDepth, 
				new DelegatingVisitor<T>(cv));
		} catch (IOException e) {
			throw new ScannerException(e);
		}
	}
}
