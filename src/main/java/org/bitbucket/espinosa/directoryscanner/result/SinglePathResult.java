package org.bitbucket.espinosa.directoryscanner.result;

import java.nio.file.Path;

public class SinglePathResult implements ScannerResult {
    private final Path path;
    
    public SinglePathResult(Path path) {
        this.path = path;
    }
    
    @Override
    public Path getPath() {
        return path;
    }
    
    @Override
    public String toString() {
        return "ScannerResult [path=" + path + "]";
    }
}
