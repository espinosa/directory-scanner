package org.bitbucket.espinosa.directoryscanner.result;

import java.nio.file.Path;

public interface ScannerResult {
    Path getPath();
}