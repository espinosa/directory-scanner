package org.bitbucket.espinosa.directoryscanner.result;

import java.nio.file.Path;
import java.util.Collection;

public class MultiPathResult implements ScannerResult {
    private final Path path;
    private final Collection<Path> attachements;

    public MultiPathResult(Path path, Collection<Path> attachements) {
        this.path = path;
        this.attachements = attachements;
    }
    
    @Override
    public Path getPath() {
        return path;
    }
    
    public Collection<Path> getSubPaths() {
        return attachements;
    }
    
    @Override
    public String toString() {
        return "ScannerResult [path=" + path + ", attachements=" + attachements + "]";
    }
}
