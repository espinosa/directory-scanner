package org.bitbucket.espinosa.directoryscanner.visitor;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import org.bitbucket.espinosa.directoryscanner.DirectoryScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DelegatingVisitor<T> extends SimpleFileVisitor<Path> {
	private final CollectingVisitor<T> cv;
	private final Logger log = LoggerFactory.getLogger(DirectoryScanner.class);
	
	public DelegatingVisitor(CollectingVisitor<T> cv) {
		this.cv = cv;
	}
	
	@Override
	public FileVisitResult preVisitDirectory(Path p, BasicFileAttributes attrs) throws IOException {
		log.trace("Visiting directory {}", p);
		if (Files.isDirectory(p) && !Files.isReadable(p)) { 
			return FileVisitResult.SKIP_SUBTREE;
		} else {
			cv.visit(p);
			return FileVisitResult.CONTINUE;   
		}
	}
	
	@Override
	public FileVisitResult visitFile(Path p, BasicFileAttributes attrs) throws IOException {
		log.trace("Visiting file {}", p);
		cv.visit(p);
		return FileVisitResult.CONTINUE;                            
	}
	
	@Override
	public FileVisitResult visitFileFailed(Path file, IOException io) {
        return FileVisitResult.SKIP_SUBTREE;
    }
}