package org.bitbucket.espinosa.directoryscanner.visitor;

import java.nio.file.Path;

public interface CollectingVisitor<T> {
	void visit(Path p);
	void setCollector(Collector<T, ?> results);
}
