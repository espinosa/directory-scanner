package org.bitbucket.espinosa.directoryscanner.visitor;

/**
 * Result collector
 * 
 * @author Espinosa
 */
public interface Collector<T, C> {
	void add(T o);
	C results();
}
