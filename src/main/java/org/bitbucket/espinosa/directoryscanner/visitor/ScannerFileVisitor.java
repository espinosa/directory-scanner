package org.bitbucket.espinosa.directoryscanner.visitor;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

import org.bitbucket.espinosa.directoryscanner.DirectoryScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Define failure as "ignore and move on" style for {@link SimpleFileVisitor}.
 */
public class ScannerFileVisitor extends SimpleFileVisitor<Path> {
	private final Logger log = LoggerFactory.getLogger(DirectoryScanner.class);

	@Override
	public FileVisitResult visitFileFailed(Path p, IOException exc) throws IOException {
		if (exc instanceof java.nio.file.AccessDeniedException)
			log.debug("Cannot access directory {}", p);
		else 
			log.warn("Error when processing directory {}", exc.toString());
		return FileVisitResult.CONTINUE;
	}
}