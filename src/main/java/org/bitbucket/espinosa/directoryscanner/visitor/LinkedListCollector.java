package org.bitbucket.espinosa.directoryscanner.visitor;

import java.util.LinkedList;
import java.util.List;

public class LinkedListCollector<T, C extends List<T>> implements Collector<T, C> {
	private final List<T> results;
	
	public LinkedListCollector() {
		this.results = new LinkedList<T>();
	}

	@Override
	public void add(T o) {
		this.results.add(o);
	}
	
	// FIXME: review generics usage 
	@SuppressWarnings("unchecked")
	@Override
	public C results() {
		return (C)this.results;
	}
}
