package org.bitbucket.espinosa.directoryscanner.visitor;

import java.nio.file.Path;

import org.bitbucket.espinosa.directoryscanner.matcher.BooleanMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.BooleanMatcherWithResults;
import org.bitbucket.espinosa.directoryscanner.matcher.GroupMatcher;
import org.bitbucket.espinosa.directoryscanner.result.MultiPathResult;
import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.bitbucket.espinosa.directoryscanner.result.SinglePathResult;

/**
 * {@link ScannerResult} collecting {@link CollectingVisitor}. It has support
 * for {@link BooleanMatcherWithResults} and actively attaches sub-results from
 * them to matching {@link ScannerResult}.
 * 
 * @author Espinosa
 */
public class SrCollectingVisitor implements CollectingVisitor<ScannerResult> {
	private final BooleanMatcher<Path> matcher;
	private Collector<ScannerResult, ?> collector;
	private final BooleanMatcherWithResults<Path, Path> srMatcher;
	
	@SuppressWarnings("unchecked")
	public SrCollectingVisitor(BooleanMatcher<Path> matcher) {
		this.matcher = matcher;
		this.srMatcher = findSubResultsMatcher(matcher);
	}

	@Override
	public void visit(Path p) {
		if (matcher.match(p)) {
			if (srMatcher != null && srMatcher.results() != null) {
			   collector.add(new MultiPathResult(p, srMatcher.results()));
			} else {
				collector.add(new SinglePathResult(p));
			}
		}
	}
	
	@Override
	public void setCollector(Collector<ScannerResult, ?> collector) {
		this.collector = collector;
	}
	
	@SuppressWarnings("rawtypes")
	private BooleanMatcherWithResults findSubResultsMatcher(BooleanMatcher m) {
		if (m instanceof BooleanMatcherWithResults) {
			return (BooleanMatcherWithResults)m;
		}
		else if (m instanceof GroupMatcher) {
			BooleanMatcher[] subMatchers = ((GroupMatcher)m).getMatchers();
			for (int i = subMatchers.length - 1; i >= 0; i--) {
				BooleanMatcherWithResults r = findSubResultsMatcher(subMatchers[i]);
				if (r != null) {
					return r;
				}
			}
		}
		return null;
	}
}
