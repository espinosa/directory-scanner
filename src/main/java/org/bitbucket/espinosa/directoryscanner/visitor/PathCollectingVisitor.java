package org.bitbucket.espinosa.directoryscanner.visitor;

import java.nio.file.Path;

import org.bitbucket.espinosa.directoryscanner.matcher.BooleanMatcher;

public class PathCollectingVisitor implements CollectingVisitor<Path> {
	private final BooleanMatcher<Path> matcher;
	//private final List<Path> results;
	private Collector<Path, ?> collector;
	
	public PathCollectingVisitor(BooleanMatcher<Path> matcher) {
		this.matcher = matcher;
	}

	@Override
	public void visit(Path p) {
		if (matcher.match(p)) {
			collector.add(p);
		}
	}

	@Override
	public void setCollector(Collector<Path, ?> collector) {
		this.collector = collector;
	}
}
