package org.bitbucket.espinosa.directoryscanner;

import java.nio.file.Path;

import org.bitbucket.espinosa.directoryscanner.matcher.AndGroupMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.BasicMatchers.EqMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.BasicMatchers.GlobMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.BasicMatchers.RegexMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.BooleanMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.SatifiableGroupMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.HasFilesMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.IsDirectoryMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.IsFileMatcher;
import org.bitbucket.espinosa.directoryscanner.matcher.OrGroupMatcher;
import org.bitbucket.espinosa.directoryscanner.result.ScannerResult;
import org.bitbucket.espinosa.directoryscanner.visitor.CollectingVisitor;
import org.bitbucket.espinosa.directoryscanner.visitor.SrCollectingVisitor;

public class Matchers {

	/** find directories where .. */
	@SafeVarargs
	public static CollectingVisitor<ScannerResult> directoryWhere(BooleanMatcher<Path>... matchers) {
		return new SrCollectingVisitor( 
		    new AndGroupMatcher<Path>(
			  new IsDirectoryMatcher(), 
			  new AndGroupMatcher<Path>(matchers)));
	}

	/** find file where .. */
	@SafeVarargs
	public static CollectingVisitor<ScannerResult> fileWhere(BooleanMatcher<Path>... matchers) {
		return new SrCollectingVisitor( 
		    new AndGroupMatcher<Path>(
			  new IsFileMatcher(), 
			  new AndGroupMatcher<Path>(matchers)));
	}
	
	/** find file or directory where .. */
	@SafeVarargs
	public static CollectingVisitor<ScannerResult> pathWhere(BooleanMatcher<Path>... matchers) {
		return new SrCollectingVisitor(
		    new AndGroupMatcher<Path>(matchers));
	}
	
	@SafeVarargs
	public static BooleanMatcher<Path> hasNamesLike(BooleanMatcher<Path>... nameMatchers) {
		return new OrGroupMatcher<Path>(nameMatchers);
	}

	@SafeVarargs
	public static BooleanMatcher<Path> hasFiles(BooleanMatcher<Path>... matchers) {
		return new HasFilesMatcher(
			new SatifiableGroupMatcher(matchers));
	}

	public static GlobMatcher fnGlob(String searchedFilePattern) {
		return new GlobMatcher(searchedFilePattern);
	}

	public static RegexMatcher fnRegex(String searchedFilePattern) {
		return new RegexMatcher(searchedFilePattern);
	}

	public static EqMatcher fnEq(String searchedFilePattern) {
		return new EqMatcher(searchedFilePattern);
	}
}
