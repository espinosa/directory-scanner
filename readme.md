Declarative File System Scanner
===============================

Originally created to find where Libre Office / Open Office is installed in the local file system and to detect where Office SDK JAR files are located within Office home directory; to be part of Maven plugin to automatically and conveniently detect this for user.

It can be used to find any application installation in any main stream operating system.
 
Implementation is based on Java 7 NIO.2 `Files.walkFileTree()` and `FileVisitor`. 

Example of usage, find `Libre Office` or `Apache Open Office` installation directory,
detect that it contain all required JARs for Office extension in Java developement:

        DirectoryScanner s = new DirectoryScanner(
                roots(root("C:/Program Files"), 
                        root("C:/Program Files (x86)"), 
                        root("/opt"), 
                        root("/usr"), 
                        root("/Applications")),
                folder(fnGlob("libreoffice*"), fnGlob("openoffice*")),
                files(fnEq("unoil.jar"), fnEq("ridl.jar"), fnEq("juh.jar"), fnEq("jurt.jar")));
        List<ScannerResult> sr = s.findAll();

This returns absolute paths for all files 

`root` is absolute path to a root directory, or directories, where to start searching from 
`fnGlob` means matching by Java 7 _glob_ template (simplified regex) 
`fnEq` means matching by filename, simple equality

The files are used to detect the right application home directory and they, their absolute paths, are also returned in the result.

And it should return something like this:

    [ScannerResult [
      path=C:\Program Files (x86)\LibreOffice 5, 
      files=[
        C:\Program Files (x86)\LibreOffice 5\program\classes\juh.jar, 
        C:\Program Files (x86)\LibreOffice 5\program\classes\jurt.jar, 
        C:\Program Files (x86)\LibreOffice 5\program\classes\ridl.jar, 
        C:\Program Files (x86)\LibreOffice 5\program\classes\unoil.jar]]]

Maturity: works for me ;)

TODO
====

 * finish (fix) regex file matcher
 * add test for home directory deeper in directory structure; search for Java home dir in existing test setup
 * add more unit tests
 * add more matchers 
 * add possibility to add max depth of search (currently hardcoded to 30)
 * add boolean parameter to all file matcher function to include/exclude that path from result

Licence
=======

Licence: Apache Licence 2.0
http://www.apache.org/licenses/LICENSE-2.0
 